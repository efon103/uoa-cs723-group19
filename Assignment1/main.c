#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include "sys/alt_irq.h"
#include "alt_types.h"                 	// alt_u32 is a kind of alt_types
#include "system.h"
#include "io.h"
#include "altera_up_avalon_video_character_buffer_with_dma.h"
#include "altera_up_avalon_video_pixel_buffer_dma.h"
#include "altera_avalon_pio_regs.h"

#include "FreeRTOS/FreeRTOS.h"
#include "FreeRTOS/task.h"
#include "FreeRTOS/queue.h"
#include "FreeRTOS/semphr.h"
#include "FreeRTOS/portmacro.h"
#include "FreeRTOS/timers.h"

#include "altera_up_avalon_ps2.h"
#include "altera_up_ps2_keyboard.h"

/*For frequency plot*/
#define FREQPLT_ORI_X 101		//x axis pixel position at the plot origin
#define FREQPLT_GRID_SIZE_X 5	//pixel separation in the x axis between two data points
#define FREQPLT_ORI_Y 199.0		//y axis pixel position at the plot origin
#define FREQPLT_FREQ_RES 20.0	//number of pixels per Hz (y axis scale)
#define ROCPLT_ORI_X 101
#define ROCPLT_GRID_SIZE_X 5
#define ROCPLT_ORI_Y 259.0
#define ROCPLT_ROC_RES 0.5		//number of pixels per Hz/s (y axis scale)
#define MIN_FREQ 45.0 //minimum frequency to draw

/*Task Priorities*/
#define PRVGADraw_Task_P    (4)
#define ANALYSER_Task_P     (4)
#define NETCON_Task_P       (3)
#define NETSTAB_Task_P      (3)
#define LOADMAN_Task_P		(4)
#define PS2_Task_P			(4)
#define Button_Task_P		(3)
#define Switch_Task_P		(2)
#define normalMode_Task_P	(3)
#define modeSwitch_Task_P	(3)
#define Timer_Reset_Task_P      (tskIDLE_PRIORITY+1)

/*Task handlers*/
TaskHandle_t PRVGADraw_t;
TaskHandle_t Timer_500_Reset;
TaskHandle_t Timer_VGA_Reset;
TaskHandle_t Timer_SW_Reset;

#define SAMPLING_FREQ 16000.0

typedef struct{
	unsigned int x1;
	unsigned int y1;
	unsigned int x2;
	unsigned int y2;
}Line;

typedef struct{ // for ps2 queue
	KB_CODE_TYPE type;
	alt_u8 key;
	char ascii;
}ps2_data;

typedef struct{ // for ps2 queue
	double freq;
	double roc;
}analyser_data;

typedef enum // for toggling between editing thresholds
{
	freq = 0,
	roc = 1,
} systemThresholds;

typedef enum // for toggling between editing thresholds
{
	waiting = 0,
	receiving = 1,
} keyboardInputs;

typedef enum // for toggling between editing thresholds
{
	stable = 0,
	unstable = 1,
} systemStatus_;

typedef enum
{
	normal = 0,
	maintenance = 1,
	halt = 2,
} mode;

/*Semaphores*/
SemaphoreHandle_t FreqAnalyser; // binary semaphore for FreqAnalyser
SemaphoreHandle_t NetCondition; // binary semaphore for FreqAnalyser
SemaphoreHandle_t ps2; // binary semaphore for FreqAnalyser
SemaphoreHandle_t button;
SemaphoreHandle_t TimerResetMutex;
SemaphoreHandle_t TimerFlagMutex;
SemaphoreHandle_t DiagMutex;
SemaphoreHandle_t VGA;
SemaphoreHandle_t loadManager;
SemaphoreHandle_t normalOperation;
SemaphoreHandle_t ledMutex;


/*Queues */
static QueueHandle_t Q_freq_data;
xQueueHandle Q_freq_calc;
xQueueHandle Q_ps2_data;
xQueueHandle Q_button_data;
xQueueHandle Q_freq_analyser;
xQueueHandle Q_net_condition;
xQueueHandle Q_Shed;
xQueueHandle Q_load;


/*Global variables*/
static int ps2_flag = 0;
static int allLoadsAdded = 0;
static int Timer500Flag = 0;
static double freqThreshold = 49;
static double rocThreshold = 20;
static systemThresholds threshold = freq;
static char currentThreshold[28] = "Freq";
static mode systemMode = normal;
static systemStatus_ systemStatus = stable;
static int minResponseTime = 9;
static int maxResponseTime = 0;
static double avgResponseTime = 0;
static double sumResponses = 0;
static int countResponses = 0;
static int runningRecord[5] = {0,0,0,0,0};
TickType_t Timer2001 = 0;
TickType_t Timer2002 = 0;
static int timingFlag = 0;
static int redLeds = 0x00;
static int greenLeds = 0x00;
static int sheddedLoads[8];
static int systemLoads[8];
static int waitForStable = 0;

/*Timers*/
TimerHandle_t timer_500;
TimerHandle_t timer_VGA;
TimerHandle_t timer_SW;

void Timer500_Reset();

/****** VGA display ******/

void PRVGADraw_Task(void *pvParameters ){


	//initialize VGA controllers
	alt_up_pixel_buffer_dma_dev *pixel_buf;
	pixel_buf = alt_up_pixel_buffer_dma_open_dev(VIDEO_PIXEL_BUFFER_DMA_NAME);
	if(pixel_buf == NULL){
		printf("can't find pixel buffer device\n");
	}
	alt_up_pixel_buffer_dma_clear_screen(pixel_buf, 0);

	alt_up_char_buffer_dev *char_buf;
	char_buf = alt_up_char_buffer_open_dev("/dev/video_character_buffer_with_dma");
	if(char_buf == NULL){
		printf("can't find char buffer device\n");
	}
	alt_up_char_buffer_clear(char_buf);



	//Set up plot axes
	alt_up_pixel_buffer_dma_draw_hline(pixel_buf, 100, 590, 200, ((0x3ff << 20) + (0x3ff << 10) + (0x3ff)), 0);
	alt_up_pixel_buffer_dma_draw_hline(pixel_buf, 100, 590, 300, ((0x3ff << 20) + (0x3ff << 10) + (0x3ff)), 0);
	alt_up_pixel_buffer_dma_draw_vline(pixel_buf, 100, 50, 200, ((0x3ff << 20) + (0x3ff << 10) + (0x3ff)), 0);
	alt_up_pixel_buffer_dma_draw_vline(pixel_buf, 100, 220, 300, ((0x3ff << 20) + (0x3ff << 10) + (0x3ff)), 0);

	alt_up_char_buffer_string(char_buf, "Group 19", 4, 0);
	alt_up_char_buffer_string(char_buf, "Press 0 to toggle between threshold modes", 4, 1);
	alt_up_char_buffer_string(char_buf, "UP and DOWN increments by 0.1, LEFT and RIGHT increments by 1.0", 4, 2);
	alt_up_char_buffer_string(char_buf, "System Time: ", 55, 0);
	alt_up_char_buffer_string(char_buf, "Frequency(Hz)", 4, 4);
	alt_up_char_buffer_string(char_buf, "52", 10, 7);
	alt_up_char_buffer_string(char_buf, "50", 10, 12);
	alt_up_char_buffer_string(char_buf, "48", 10, 17);
	alt_up_char_buffer_string(char_buf, "46", 10, 22);

	alt_up_char_buffer_string(char_buf, "df/dt(Hz/s)", 4, 26);
	alt_up_char_buffer_string(char_buf, "60", 10, 28);
	alt_up_char_buffer_string(char_buf, "30", 10, 30);
	alt_up_char_buffer_string(char_buf, "0", 10, 32);
	alt_up_char_buffer_string(char_buf, "-30", 9, 34);
	alt_up_char_buffer_string(char_buf, "-60", 9, 36);


	alt_up_char_buffer_string(char_buf, "Threshold Freq (Hz):", 4, 42);
	alt_up_char_buffer_string(char_buf, "Threshold ROC (Hz/s):", 40, 42);
	alt_up_char_buffer_string(char_buf, "Input Mode:", 4, 44);
	alt_up_char_buffer_string(char_buf, "Maintenance Mode:", 40, 44);
	alt_up_char_buffer_string(char_buf, "5 readings: ", 4, 54);
	alt_up_char_buffer_string(char_buf, "Max time: ", 4, 48);
	alt_up_char_buffer_string(char_buf, "Min time: ", 40, 48);
	alt_up_char_buffer_string(char_buf, "Average time: ", 4, 50);
	alt_up_char_buffer_string(char_buf, "System Status: ", 40, 50);

	double freq[100], dfreq[100];
	int i = 99, j = 0;
	Line line_freq, line_roc;
	char roc[10] = "";
	char frequency[10] = "";
	char systemTime[10] = "";
	char max[10] = "";
	char min[10] = "";
	char avg[10] = "";
	char reading1[10] = "";
	char reading2[10] = "";
	char reading3[10] = "";
	char reading4[10] = "";
	char reading5[10] = "";
	TickType_t tickCount;

	while(1){
		alt_up_char_buffer_string(char_buf, currentThreshold, 16, 44); // print editing current threshold
		sprintf(roc,"%.2f",rocThreshold);
		sprintf(frequency,"%.2f",freqThreshold);
		tickCount = xTaskGetTickCount()/1000; // convert to secs
		sprintf(systemTime,"%u secs",tickCount);
		alt_up_char_buffer_string(char_buf, frequency, 25, 42); // prints frequency on screen
		alt_up_char_buffer_string(char_buf, roc, 62, 42); // prints roc on screen
		alt_up_char_buffer_string(char_buf, systemTime, 68, 0); // prints system time on screen

		sprintf(max,"%u ms",maxResponseTime);
		alt_up_char_buffer_string(char_buf, max, 14, 48); // prints frequency on screen
		sprintf(min,"%u ms",minResponseTime);
		alt_up_char_buffer_string(char_buf, min, 50, 48); // prints frequency on screen
		sprintf(avg,"%.2f ms",avgResponseTime);
		alt_up_char_buffer_string(char_buf, avg, 18, 50); // prints frequency on screen

		sprintf(reading5,"%d ms   ",runningRecord[0]);
		alt_up_char_buffer_string(char_buf, reading1, 16, 54);
		sprintf(reading4,"%d ms, ",runningRecord[1]);
		alt_up_char_buffer_string(char_buf, reading2, 24, 54);
		sprintf(reading3,"%d ms, ",runningRecord[2]);
		alt_up_char_buffer_string(char_buf, reading3, 32, 54);
		sprintf(reading2,"%d ms, ",runningRecord[3]);
		alt_up_char_buffer_string(char_buf, reading4, 40, 54);
		sprintf(reading1,"%d ms, ",runningRecord[4]);
		alt_up_char_buffer_string(char_buf, reading5, 48, 54);

		if(systemStatus == stable){
			alt_up_char_buffer_string(char_buf, "Stable  ", 55, 50);
		} else if (systemStatus == unstable){
			alt_up_char_buffer_string(char_buf, "Unstable", 55, 50);
		}

		if (waitForStable == 1){
			alt_up_char_buffer_string(char_buf, "Pending...", 60, 44);
		} else if (systemMode == normal){
			alt_up_char_buffer_string(char_buf, "OFF       ", 60, 44);
		} else if (systemMode == maintenance){
			alt_up_char_buffer_string(char_buf, "ON        ", 60, 44);
		}

		//receive frequency data from queue
		//if(xSemaphoreTake(VGA, portMAX_DELAY)== pdTRUE){
			while(uxQueueMessagesWaiting( Q_freq_data ) != 0){
				xQueueReceive( Q_freq_data, freq+i, 0 );

				//calculate frequency RoC

				if(i==0){
					dfreq[0] = (freq[0]-freq[99]) * 2.0 * freq[0] * freq[99] / (freq[0]+freq[99]); // diff*2*f[0]*f[99]
				}
				else{
					dfreq[i] = (freq[i]-freq[i-1]) * 2.0 * freq[i]* freq[i-1] / (freq[i]+freq[i-1]);
				}

				if (dfreq[i] > 100.0){
					dfreq[i] = 100.0;
				}


				i =	++i%100; //point to the next data (oldest) to be overwritten

			}


			//clear old graph to draw new graph
			alt_up_pixel_buffer_dma_draw_box(pixel_buf, 101, 0, 639, 199, 0, 0);
			alt_up_pixel_buffer_dma_draw_box(pixel_buf, 101, 201, 639, 299, 0, 0);

			for(j=0;j<99;++j){ //i here points to the oldest data, j loops through all the data to be drawn on VGA
				if (((int)(freq[(i+j)%100]) > MIN_FREQ) && ((int)(freq[(i+j+1)%100]) > MIN_FREQ)){
					//Calculate coordinates of the two data points to draw a line in between
					//Frequency plot
					line_freq.x1 = FREQPLT_ORI_X + FREQPLT_GRID_SIZE_X * j;
					line_freq.y1 = (int)(FREQPLT_ORI_Y - FREQPLT_FREQ_RES * (freq[(i+j)%100] - MIN_FREQ));

					line_freq.x2 = FREQPLT_ORI_X + FREQPLT_GRID_SIZE_X * (j + 1);
					line_freq.y2 = (int)(FREQPLT_ORI_Y - FREQPLT_FREQ_RES * (freq[(i+j+1)%100] - MIN_FREQ));

					//Frequency RoC plot
					line_roc.x1 = ROCPLT_ORI_X + ROCPLT_GRID_SIZE_X * j;
					line_roc.y1 = (int)(ROCPLT_ORI_Y - ROCPLT_ROC_RES * dfreq[(i+j)%100]);

					line_roc.x2 = ROCPLT_ORI_X + ROCPLT_GRID_SIZE_X * (j + 1);
					line_roc.y2 = (int)(ROCPLT_ORI_Y - ROCPLT_ROC_RES * dfreq[(i+j+1)%100]);

					//Draw
					alt_up_pixel_buffer_dma_draw_line(pixel_buf, line_freq.x1, line_freq.y1, line_freq.x2, line_freq.y2, 0x3ff << 0, 0);
					alt_up_pixel_buffer_dma_draw_line(pixel_buf, line_roc.x1, line_roc.y1, line_roc.x2, line_roc.y2, 0x3ff << 0, 0);
				}
			}
		//}
		vTaskDelay(20);

	}
}

void freq_relay(){
	double freq = SAMPLING_FREQ/(double)IORD(FREQUENCY_ANALYSER_BASE, 0); // Frequency - WE NEED ROC
	xQueueSendToBackFromISR( Q_freq_data, &freq, pdFALSE ); // vga
	xQueueSendToBackFromISR( Q_freq_calc, &freq, pdFALSE ); // calculator
	xSemaphoreGiveFromISR(FreqAnalyser, NULL); // wake up task to do calculations on ASAP
	return;
}


/*Frequency Analyser Task
 *Calculate and store frequency value and ROC
 *Send values to NetConditions via queue  */
void FreqAnalyser_Task(void* context){
	analyser_data queue_data;
	double freqNew = 50.0;
	double freqOld = 50.0;
	double ROC = 0.00;
	while(1){
		if(xSemaphoreTake(FreqAnalyser, portMAX_DELAY)){// wait for ISR to give the binary semaphore
			//Do calculations
			xQueueReceive(Q_freq_calc, &freqNew, 0); // queue_name, &rx, ticks_to_wait
			ROC = ((freqNew-freqOld)*SAMPLING_FREQ)/(double)IORD(FREQUENCY_ANALYSER_BASE,0); // calc ROC
			freqOld = freqNew; // update old - then send to
			queue_data.freq = freqNew;
			queue_data.roc = ROC;

			xQueueSend(Q_freq_analyser, &queue_data, 0); // send data to the queue
			xSemaphoreGive(NetCondition); // wake up task to do calculations on ASAP
		}

	}
}

void NetworkCondition_Task(void* context){
	//double freq[100], dfreq[100], temp;
	analyser_data queue_data;
	double freq, roc = 0;
	int stable = 1; // unstable = 0, stable = 1; - this could be a global variable
	while(1){
		if(xSemaphoreTake(NetCondition, portMAX_DELAY)){// wait for ISR to give the binary semaphore
			xQueueReceive(Q_freq_analyser, &queue_data, 0); // queue_name, &rx, ticks_to_wait
			freq = queue_data.freq;
			roc = fabs(queue_data.roc); // to get absolute value
			if(freq < freqThreshold || roc > rocThreshold){ //Do comparison with threshold values
				stable = 0;
				if(timingFlag == 0){
					Timer2001 = xTaskGetTickCount(); // start 200ms timer
					printf("Detection Time: %u\n", Timer2001);
					xSemaphoreTake(DiagMutex, 10);
					timingFlag = 1;
					xSemaphoreGive(DiagMutex);
				}
			} else{
				stable = 1;
				if (allLoadsAdded == 1){
					xSemaphoreGive(normalOperation);
				}
			}
			xQueueSend(Q_net_condition, &stable, 0); // send data to the queue
		}
	}
}

void NetworkStabiliser_Task(void* context){
	int newStable = 1; // unstable = 0, stable = 1; - this could be a global variable
	int oldStable = 1;
	int firstShed = 0;
	int shedOut; // 0 = shed, 1 = add
	while(1){
		while(uxQueueMessagesWaiting( Q_net_condition ) != 0){ // while there is data in queue
			xQueueReceive(Q_net_condition, &newStable, 0); // queue_name, &rx, ticks_to_wait
			switch (newStable){
				case 0:
					systemStatus = unstable;
					break;
				case 1:
					systemStatus = stable;
					break;
			}
			if (systemMode == normal){
				if(allLoadsAdded == 1 && newStable == 1){
					firstShed = 0; // reset first shed flag
					xSemaphoreTake(DiagMutex, 10);
					timingFlag = 0;
					xSemaphoreGive(DiagMutex);
				}
				if (newStable == 0 && firstShed == 0){ // first shed (immediate shed)
					shedOut = 0; //shed
					xQueueSend(Q_Shed, &shedOut, 0); // send data to the queue - load manager task can just wait for a message in queue
					printf("First Shed\n");
					firstShed = 1;
					xSemaphoreGive(loadManager);
				}
				if (newStable != oldStable){ //reset timer if state changes
					Timer500_Reset();
				}
			}
			oldStable = newStable;
		}

		if(Timer500Flag == 1){
			switch (newStable){
				case 0:
					systemStatus = unstable;
					shedOut = 0; //shed
					xQueueSend(Q_Shed, &shedOut, 0); // send data to the queue - load manager task can just wait for a message in queue
					xSemaphoreGive(loadManager);
					break;
				case 1:
					systemStatus = stable;
					shedOut = 1; //add
					xQueueSend(Q_Shed, &shedOut, 0); // send data to the queue - load manager task can just wait for a message in queue
					xSemaphoreGive(loadManager);
					break;
			}
			// create mutex
			xSemaphoreTake(TimerFlagMutex,10);
			Timer500Flag = 0;
			xSemaphoreGive(TimerFlagMutex);
		}
	}
}

/*Load Manager Task
 *Manage Loads
 *Send time values to TimingManager via queue  */
void LoadManager_Task(void* context){
	int shed;
	int greenLeds = 0x00;
	redLeds = IORD_ALTERA_AVALON_PIO_DATA(SLIDE_SWITCH_BASE) & 0x00FF;

	int n; //initialise empty shed state
	for (n = 0; n < 8; n++){
		sheddedLoads[n] = 0; //0 = hasn't been shedded, 1 = has been shedded
	}

	while(1){
		if(xSemaphoreTake(loadManager, portMAX_DELAY)){// from switch task
			if(uxQueueMessagesWaiting(Q_Shed) != 0){
				xQueueReceive(Q_Shed, &shed, 0);
				if(systemMode == normal){ //if normal mode, take led mutex
					xSemaphoreTake(ledMutex,10);
					if (shed == 0){	//shed from lowest to highest priority
						if (systemLoads[0] == 1 && sheddedLoads[0] == 0){
							greenLeds |= 0x01;
							redLeds &= ~0x01;
							sheddedLoads[0] = 1;
						} else if (systemLoads[1] == 1 && sheddedLoads[1] == 0){
							greenLeds |= 0x02;
							redLeds &= ~0x02;
							sheddedLoads[1] = 1;
						} else if (systemLoads[2] == 1 && sheddedLoads[2] == 0){
							greenLeds |= 0x04;
							redLeds &= ~0x04;
							sheddedLoads[2] = 1;
						} else if (systemLoads[3] == 1 && sheddedLoads[3] == 0){
							greenLeds |= 0x08;
							redLeds &= ~0x08;
							sheddedLoads[3] = 1;
						} else if (systemLoads[4] == 1 && sheddedLoads[4] == 0){
							greenLeds |= 0x10;
							redLeds &= ~0x10;
							sheddedLoads[4] = 1;
						} else if (systemLoads[5] == 1 && sheddedLoads[5] == 0){
							greenLeds |= 0x20;
							redLeds &= ~0x20;
							sheddedLoads[5] = 1;
						} else if (systemLoads[6] == 1 && sheddedLoads[6] == 0){
							greenLeds |= 0x40;
							redLeds &= ~0x40;
							sheddedLoads[6] = 1;
						} else if (systemLoads[7] == 1 && sheddedLoads[7] == 0){
							greenLeds |= 0x80;
							redLeds &= ~0x80;
							sheddedLoads[7] = 1;
						}

						if(timingFlag == 1){ //diagnostic timer
							Timer2002 = xTaskGetTickCount(); // checking time difference
							unsigned int timeDiff = (Timer2002 - Timer2001); // in ms

							if (timeDiff > maxResponseTime){
								maxResponseTime = timeDiff;
							}
							if (timeDiff < minResponseTime){
								minResponseTime = timeDiff;
							}

							countResponses++; //calculate running average
							sumResponses += timeDiff;
							avgResponseTime = sumResponses / (double) countResponses;

							xSemaphoreTake(DiagMutex, 10); //remove flag to stop diagnostic timer
							timingFlag = 2;
							xSemaphoreGive(DiagMutex);
							int n; //array with last 5 values
							for(n = 0; n < 4; n++){ // shift array
								runningRecord[n] = runningRecord[n+1];
							}
							runningRecord[4] = timeDiff;
						}
					} else{ // add loads from highest to lowest priority
						if (sheddedLoads[7] == 1){
							if (systemLoads[7] == 1){
								redLeds |= 0x80;
							} else {
								redLeds &= ~0x80;
							}
							greenLeds &= ~0x80;
							sheddedLoads[7] = 0;
						} else if (sheddedLoads[6] == 1){
							if (systemLoads[6] == 1){
								redLeds |= 0x40;
							} else {
								redLeds &= ~0x40;
							}
							greenLeds &= ~0x40;
							sheddedLoads[6] = 0;
						} else if (sheddedLoads[5] == 1){
							if (systemLoads[5] == 1){
								redLeds |= 0x20;
							} else {
								redLeds &= ~0x20;
							}
							greenLeds &= ~0x20;
							sheddedLoads[5]= 0;
						} else if (sheddedLoads[4] == 1){
							if (systemLoads[4] == 1){
								redLeds |= 0x10;
							} else {
								redLeds &= ~0x10;
							}
							greenLeds &= ~0x10;
							sheddedLoads[4] = 0;
						} else if (sheddedLoads[3] == 1){
							if (systemLoads[3] == 1){
								redLeds |= 0x08;
							} else {
								redLeds &= ~0x08;
							}
							greenLeds &= ~0x08;
							sheddedLoads[3] = 0;
						} else if (sheddedLoads[2] == 1){
							if (systemLoads[2] == 1){
								redLeds |= 0x04;
							} else {
								redLeds &= ~0x04;
							}
							greenLeds &= ~0x04;
							sheddedLoads[2] = 0;
						} else if (sheddedLoads[1] == 1){
							if (systemLoads[1] == 1){
								redLeds |= 0x02;
							} else {
								redLeds &= ~0x02;
							}
							greenLeds &= ~0x02;
							sheddedLoads[1] = 0;
						} else if (sheddedLoads[0] == 1){
							if (systemLoads[0] == 1){
								redLeds |= 0x01;
							} else {
								redLeds &= ~0x01;
							}
							greenLeds &= ~0x01;
							sheddedLoads[0] = 0;
						}
					} //write red and green values to leds
					IOWR_ALTERA_AVALON_PIO_DATA(GREEN_LEDS_BASE, greenLeds);
					IOWR_ALTERA_AVALON_PIO_DATA(RED_LEDS_BASE, redLeds);

					// check if all loads have been added.. if so write to global variable = 1 if not = 0
					if(sheddedLoads[7] == 0 && sheddedLoads[6] == 0 && sheddedLoads[5] == 0 && sheddedLoads[4] == 0 &&
					   sheddedLoads[3] == 0 && sheddedLoads[2] == 0 && sheddedLoads[1] == 0 && sheddedLoads[0] == 0){
						allLoadsAdded = 1;
						Timer500_Reset();
						xTimerStop(timer_500,10);
					} else {
						allLoadsAdded = 0;
					}
				}
			}
			xSemaphoreGive(ledMutex);
		}
	}
}

void normalMode(void* context){
	int temp;

	while(1){
		if(uxQueueMessagesWaiting(Q_load) != 0){
			xQueueReceive(Q_load, &temp, 0);

			systemLoads[0] = temp & 1;
			temp >>= 1;
			systemLoads[1] = temp & 1;
			temp >>= 1;
			systemLoads[2] = temp & 1;
			temp >>= 1;
			systemLoads[3] = temp & 1;
			temp >>= 1;
			systemLoads[4] = temp & 1;
			temp >>= 1;
			systemLoads[5] = temp & 1;
			temp >>= 1;
			systemLoads[6] = temp & 1;
			temp >>= 1;
			systemLoads[7] = temp & 1;


			if (systemMode == maintenance || allLoadsAdded == 1){ //in maintenance mode, wall switches are free to change
				xSemaphoreTake(ledMutex,10);
				redLeds = systemLoads[0] + systemLoads[1]*2 + systemLoads[2]*4 + systemLoads[3]*8 +
						  systemLoads[4]*16 + systemLoads[5]*32 + systemLoads[6]*64 + systemLoads[7]*128;
				greenLeds = 0x00;
				IOWR_ALTERA_AVALON_PIO_DATA(RED_LEDS_BASE, redLeds);
				IOWR_ALTERA_AVALON_PIO_DATA(GREEN_LEDS_BASE, greenLeds);
				xSemaphoreGive(ledMutex);
			}
		}
	}
}

void ps2_isr (void* context)
{
  int status = 0;
  ps2_data packet;
  status = decode_scancode (context, &packet.type , &packet.key , &packet.ascii) ;
  if ( status == 0 && ps2_flag == 0) //success
  {
	ps2_flag = 1;
	xQueueSendToBackFromISR( Q_ps2_data, &packet, pdFALSE );
	static signed xHigherPriorityWoken;
	xHigherPriorityWoken = pdFALSE;
	xSemaphoreGiveFromISR(ps2, &xHigherPriorityWoken); // wake up task to do calculations on ASAP
	portEND_SWITCHING_ISR(xHigherPriorityWoken);
  }
  else if (status == 0 && ps2_flag == 1){
	ps2_flag = 0;

  }
}

/*To update thresholds*/
void ps2_task(void* context){
	ps2_data packet;
	while(1){
		if(xSemaphoreTake(ps2, portMAX_DELAY)== pdTRUE){
			xQueueReceive(Q_ps2_data, &packet, 0);
			switch ( packet.type ){
			  case KB_ASCII_MAKE_CODE :
				//if(keyboardInput != receiving){
					if(packet.ascii == '0'){
						if(threshold == freq){
							threshold = roc;
							memset(currentThreshold, "", 28);
							strncpy(currentThreshold, "ROC ", 28);
						} else {
							threshold = freq;
							memset(currentThreshold, "", 28);
							strncpy(currentThreshold, "Freq", 28);
						}
					} else if(((int)packet.ascii >= 48) && ((int)packet.ascii <= 57)){ // 0 is 48 9 is 57 - only want to do numbers
						if((int)packet.ascii == 56){ // up
							if(threshold == freq && freqThreshold >= 0.0){
								freqThreshold += 0.10;
							} else if(threshold == roc && rocThreshold >= 0.0){
								rocThreshold += 0.10;
							}
						} else if((int)packet.ascii == 50){ // down
							if(threshold == freq && freqThreshold >= 0.1){
								freqThreshold -= 0.10;
							} else if(threshold == roc && rocThreshold >= 0.1){
								rocThreshold -= 0.10;
							}
						} else if((int)packet.ascii == 52){ // left
							if(threshold == freq && freqThreshold >= 1.0){
								freqThreshold -= 1.00;
							} else if(threshold == roc && rocThreshold >= 1.0){
								rocThreshold -= 1.00;
							}
						}
						else if((int)packet.ascii == 54){ // right
							if(threshold == freq && freqThreshold >= 0.0){
								freqThreshold += 1.00;
							} else if(threshold == roc && rocThreshold >= 0.0){
								rocThreshold += 1.00;
							}
						}
					}
				break ;
			  case KB_LONG_BINARY_MAKE_CODE :
				// do nothing
			  case KB_BINARY_MAKE_CODE :
				break ;
			  case KB_BREAK_CODE :
				// do nothing
			  default :
				printf ( "DEFAULT   : %c\n", packet.ascii ) ;
				break ;
			}
		}
	}
}

void button_isr(void* context, alt_u32 id)
{
	// need to cast the context first before using it
	int temp = (int*) context;
	temp = IORD_ALTERA_AVALON_PIO_EDGE_CAP(PUSH_BUTTON_BASE);

	// clears the edge capture register
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PUSH_BUTTON_BASE, 0x7);
	xQueueSendToBackFromISR( Q_button_data, &temp, pdFALSE );

	static signed xHigherPriorityWoken2;
	xHigherPriorityWoken2 = pdFALSE;
	xSemaphoreGiveFromISR(button, &xHigherPriorityWoken2); // wake up task to do calculations on ASAP
	portEND_SWITCHING_ISR(xHigherPriorityWoken2);
}


//NOTE: Unable to read and clear button register in task.
void button_task(void* context){
	int buttonValue = 0;
	mode previousState;

	while(1){
		if(xSemaphoreTake(button, portMAX_DELAY)== pdTRUE){
			xQueueReceive(Q_button_data, &buttonValue, 0);
			if (buttonValue == 1){
				waitForStable = 1; //make sure mode is safe before changing to a different mode
			}
			else if (buttonValue == 2){ //pause everything
				switch (systemMode){
					case (normal):
						previousState = systemMode;
						systemMode = halt;
						break;
					case (maintenance):
						previousState = systemMode;
						systemMode = halt;
						break;
					case (halt):
						systemMode = previousState;
						break;
				}
			}
		}
	}
}

void modeSwitch_Task(void * context){
	while(1){
		if (waitForStable == 1 && allLoadsAdded == 1){ //if button is pressed out of allLoadsAdded state, wait
			switch (systemMode){
				case (normal):
					systemMode = maintenance;
					Timer2002 = xTaskGetTickCount();
					break;
				case (maintenance):
					systemMode = normal;
					break;
				case (halt):
					break;
			}
			waitForStable = 0;
		}
	}
}

void switch_task(void* context){
	unsigned int uiSwitchValue =  IORD_ALTERA_AVALON_PIO_DATA(SLIDE_SWITCH_BASE) & 0x00FF; // mask the last 8 LEDs
	// check system mode
	// check systemMode: if normal, only add/shed when stable? when maintenance then just send
	xQueueSend(Q_load, &uiSwitchValue, 0);
	if(systemMode == normal){
		xSemaphoreGive(loadManager);
	}
}

void vTimer500Callback(xTimerHandle t_timer){ //Timer flashes red LEDs
	xSemaphoreTake(TimerFlagMutex,10);
	Timer500Flag = 1;
	xSemaphoreGive(TimerFlagMutex);
}

void Timer500_Reset(void *pvParameters ){ //reset timer if first load shed happens
	xSemaphoreTake(TimerFlagMutex,10);
	Timer500Flag = 0;
	xSemaphoreGive(TimerFlagMutex);

	xTimerReset( timer_500, 10 );
	return;
}

void Timer_500_Reset_Task(void *pvParameters ){ //reset timer if first load shed happens
	while(1){}
}

void Timer_VGA_Reset_Task(void *pvParameters ){ //reset timer if first load shed happens
	while(1){}
}

void Timer_SW_Reset_Task(void *pvParameters ){ //reset timer if first load shed happens
	while(1){}
}

void vTimerVGACallback(xTimerHandle t_timer){ //Timer flashes r LEDs
}

int main() // test
{
	void * buttonContext;
	/*Interrupts*/
	alt_irq_register(FREQUENCY_ANALYSER_IRQ, 0, freq_relay);

	/*Semaphore*/
	FreqAnalyser = xSemaphoreCreateBinary(); // Binary semaphore for FreqAnalyserIRQ
	ps2 = xSemaphoreCreateBinary();
	button = xSemaphoreCreateBinary();
	NetCondition = xSemaphoreCreateBinary();
	VGA = xSemaphoreCreateBinary();
	loadManager = xSemaphoreCreateBinary();
	TimerResetMutex = xSemaphoreCreateMutex();
	TimerFlagMutex = xSemaphoreCreateMutex();
	DiagMutex = xSemaphoreCreateMutex();
	ledMutex = xSemaphoreCreateMutex();
	normalOperation = xSemaphoreCreateBinary();

	alt_up_ps2_dev * ps2_device = alt_up_ps2_open_dev(PS2_NAME);

	  if(ps2_device == NULL){
	    printf("can't find PS/2 device\n");
	    return 1;
	  }

	  alt_up_ps2_clear_fifo (ps2_device) ;

	  alt_irq_register(PS2_IRQ, ps2_device, ps2_isr);
	  // register the PS/2 interrupt
	  IOWR_8DIRECT(PS2_BASE,4,1);

	// clears the edge capture register. Writing 1 to bit clears pending interrupt for corresponding button.
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PUSH_BUTTON_BASE, 0x7);

	// enable interrupts for all buttons
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(PUSH_BUTTON_BASE, 0x7);

	// register the ISR
	alt_irq_register(PUSH_BUTTON_IRQ, buttonContext, button_isr);

	Q_freq_data = xQueueCreate( 100, sizeof(double) ); // queue for VGA Display
	Q_freq_calc = xQueueCreate( 20, sizeof (double) ); // queue for Calculation
	Q_ps2_data = xQueueCreate( 2, sizeof(KB_CODE_TYPE)+(2*sizeof(char)) ); // queue for VGA DisplayQ-
	Q_button_data = xQueueCreate( 10, sizeof(int) ); // queue for buttons
	Q_freq_analyser = xQueueCreate( 10, sizeof(analyser_data) ); // queue for buttons
	Q_net_condition = xQueueCreate( 10, sizeof(int) ); // queue for buttons
	Q_Shed = xQueueCreate( 10, sizeof(int) ); // queue for add/shed
	Q_load = xQueueCreate( 10, sizeof(int) ); // queue for add/shed

	//alt_irq_register(FREQUENCY_ANALYSER_IRQ, 0, freq_relay);
	timer_500 = xTimerCreate("500ms Timer", 500, pdTRUE, NULL, vTimer500Callback);
	timer_SW = xTimerCreate("SW Timer", 100, pdTRUE, NULL, switch_task);
	xTimerStop(timer_500,10); // stop timer as it doesn't need to be started yet
	timer_VGA = xTimerCreate("VGAms Timer", 20, pdTRUE, NULL, vTimerVGACallback);

		if (xTimerStart(timer_500, 0) != pdPASS){
			printf("Cannot start 500ms timer");
		}
		if (xTimerStart(timer_SW, 0) != pdPASS){
			printf("Cannot start SW timer");
		}
		if (xTimerStart(timer_VGA, 0) != pdPASS){
			printf("Cannot start VGAms timer");
		}

	xTaskCreate( Timer_500_Reset_Task, "0", configMINIMAL_STACK_SIZE, NULL, Timer_Reset_Task_P, &Timer_500_Reset );
	xTaskCreate( Timer_SW_Reset_Task, "0", configMINIMAL_STACK_SIZE, NULL, Timer_Reset_Task_P, &Timer_SW_Reset );
	xTaskCreate( Timer_VGA_Reset_Task, "0", configMINIMAL_STACK_SIZE, NULL, Timer_Reset_Task_P, &Timer_VGA_Reset );
	xTaskCreate( PRVGADraw_Task, "DrawTsk", configMINIMAL_STACK_SIZE, NULL, PRVGADraw_Task_P, &PRVGADraw_t); // Create VGA task
	xTaskCreate( FreqAnalyser_Task, "FreqAnalyserTsk", configMINIMAL_STACK_SIZE, NULL, ANALYSER_Task_P, NULL );
	xTaskCreate( ps2_task, "PSTsk", configMINIMAL_STACK_SIZE, NULL, PS2_Task_P, NULL );
	xTaskCreate( button_task, "ButtonTsk", configMINIMAL_STACK_SIZE, NULL, Button_Task_P, NULL );
	xTaskCreate( normalMode, "normalModeTsk", configMINIMAL_STACK_SIZE, NULL, normalMode_Task_P, NULL ); // it is now in a timer
	xTaskCreate( NetworkCondition_Task, "NetworkConditionsTsk", configMINIMAL_STACK_SIZE, NULL, NETCON_Task_P, NULL );
	xTaskCreate( NetworkStabiliser_Task, "NetworkStabiliserTsk", configMINIMAL_STACK_SIZE, NULL, NETSTAB_Task_P, NULL );
	xTaskCreate( LoadManager_Task, "LoadManagerTsk", configMINIMAL_STACK_SIZE, NULL, LOADMAN_Task_P, NULL );
	xTaskCreate( modeSwitch_Task, "modeSwitchTsk", configMINIMAL_STACK_SIZE, NULL, modeSwitch_Task_P, NULL );

	vTaskStartScheduler();
	taskENABLE_INTERRUPTS();

	while(1)

  return 0;
}

